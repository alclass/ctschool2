from django.db import models
from django.contrib import admin
from django.conf import settings
from photologue.models import Photo

class MedicalFacility(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name
	class Meta:
		verbose_name_plural = "Medical Facilities"
		
class MedicalFacilityAdmin(admin.ModelAdmin):
    list_display = ['name']

		
class Person(models.Model):
    prefix = models.CharField(max_length=200, verbose_name = "Academic title", blank=True, default="")
    name = models.CharField(max_length=200, verbose_name = "Name")
    surname = models.CharField(max_length=200, verbose_name = "Surname")
    birthday = models.DateField(verbose_name = "Birth date", blank=True, null=True)
    employer = models.ForeignKey(MedicalFacility, verbose_name = "Employer", blank=True, null=True)
    photo = models.ForeignKey(Photo, blank=True, null=True)
    email = models.EmailField(verbose_name = "Email", blank = True)
    isDoctor = models.BooleanField(verbose_name = "Doctor?")
    isIntern = models.BooleanField(verbose_name = "Intern?")
    isRadiologyst = models.BooleanField(verbose_name = "Radiologyst?")
    def __str__(self):
        return self.prefix + " " + self.name + " " + self.surname
	def fullname(self):
		return self.prefix + " " + self.name + " " + self.surname + ", " + self.employer.name
	
		
class Participant(Person):
    pass

class TeamMember(Person):
    toDisplay = models.BooleanField(verbose_name = "Display at Team page?", help_text="Set this value to True if you want this person to appear on Team page", default = True)
    desc = models.TextField(verbose_name = "Short bio", blank=True)
	
class ParticipantAdmin(admin.ModelAdmin):
	list_display = ('name', 'surname', 'employer', 'isDoctor', 'isIntern', 'isRadiologyst')

class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'employer', 'isDoctor', 'isIntern', 'isRadiologyst', 'desc')
    fieldsets = (
        ('Instructions', {	
			'fields': (),	
			'classes': ('collapse',),			
			'description': '<ul><li>On this page You can insert new of change details of existing team member.</li><li>When You create new member, You can assign any photo to new member. Please add team photos to Team Gallery</li><li> Team Members checked to Display will automatically appear on Site->Team page. </li><li>Event details from \'Advances Options\' can be left empty.</li><li> You can change ALL options like short bio or photo whenever you want. </li><li>It\'s YOUR responsibility to enter valid data. </li><li>Enter date in format YYYY-MM-DD For Example: 2008-06-25</li></ul>',
			}),
        ('Basic Options', {
			'fields': ('prefix', 'name','surname'),			
			'description': 'These options have to be filled to insert new team member into database',
			}),
		('Advanced Options', {
			'fields': ('birthday','employer','photo','email','desc','toDisplay','isDoctor','isIntern','isRadiologyst'),
			'description': 'You can fill this fields later',
			}),
    )
    class Media:
		js = ("/static/js/tiny_mce/tiny_mce.js",
		      "/static/js/tiny_mce/textareas.js",)
	
	
	
admin.site.register(Participant, ParticipantAdmin)
admin.site.register(TeamMember, TeamMemberAdmin)
admin.site.register(MedicalFacility, MedicalFacilityAdmin)
