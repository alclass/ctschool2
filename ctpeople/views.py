from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Template, Context
from django.shortcuts import render_to_response
from ctschool.ctpeople.models import TeamMember
from ctschool.ctfriends.models import Friend

def team_list(request):
    icontext = Context()
    team = TeamMember.objects.filter(toDisplay=True)
    icontext['team'] = team;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    icontext['layouttype'] = 'fixed'
    return render_to_response('team_list.html', icontext)