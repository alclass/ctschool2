from django.conf import settings
from django.conf.urls.defaults import *

#events
urlpatterns = patterns('',
  url(r'', include('ctschool.ctbooks.urls')),		
  url(r'', include('ctschool.ctevents.urls')),    
  # url(r'', include('ctfriends.urls')),    
  url(r'', include('ctschool.ctnews.urls')),    
  url(r'', include('ctschool.ctpeople.urls')),
  url(r'', include('ctschool.photologue.urls')),    
)
