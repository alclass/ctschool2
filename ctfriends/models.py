from django.db import models
from django.contrib import admin
from django.conf import settings
from photologue.models import Photo


class Friend(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Friend name")
    url = models.URLField(verbose_name = "Site URL")
    photo = models.ForeignKey(Photo)
    isSponsor = models.BooleanField(verbose_name = "Sponsor?")
    isFriend = models.BooleanField(verbose_name = "Friend?")
    isGeneralSponsor = models.BooleanField(verbose_name = "General sponsor?")
    photo = models.ForeignKey(Photo, related_name='employee_profiles')
	
class FriendAdmin(admin.ModelAdmin):
    list_display = ('title', 'isSponsor', 'isFriend', 'isGeneralSponsor')	

admin.site.register(Friend, FriendAdmin)