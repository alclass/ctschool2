from django.conf import settings
from django.conf.urls.defaults import *

#events
urlpatterns = patterns('',
	url(r'^$', 'ctschool.ctbooks.views.book_list', name='book_list'),		
    url(r'^(?P<bookID>\d+)/$', 'ctschool.ctbooks.views.book_detail', name='book_detail'),		
)