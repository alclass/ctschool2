from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Template, Context
from django.shortcuts import render_to_response
from ctschool.ctfriends.models import Friend
from ctschool.ctbooks.models import Book

def book_list(request):
    icontext = Context()
    books = Book.objects.all()
    icontext['books'] = books;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    icontext['layouttype'] = 'fixed'
    return render_to_response('book_list.html', icontext)

def book_detail(request, bookID):
    icontext = Context()
    book = Book.objects.get(id=bookID)
    icontext['book'] = book;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    icontext['layouttype'] = 'fixed'
    return render_to_response('book_detail.html', icontext)