from django.db import models
from django.contrib import admin
from django.conf import settings
from ctschool.ctpeople.models import Person

class Book(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Book title")
    subtitle = models.CharField(max_length=200, verbose_name = "Book subtitle")
    shorttitle = models.CharField(max_length=25, verbose_name = "Short title")
    year = models.IntegerField(verbose_name = "Year Published")
    authors = models.ManyToManyField(Person, verbose_name = "Authors")
    desc = models.TextField(verbose_name = "Book description")
    def __str__(self):
        return self.title + "," + str(self.year)
    def get_absolute_url(self):
        return "/book/%i/" % self.id

class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle', 'year', 'authors')	
    class Media:
        js = ("/static/js/tiny_mce/tiny_mce.js",
              "/static/js/tiny_mce/textareas.js",)
			  
admin.site.register(Book)