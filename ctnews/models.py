from django.db import models
from django.contrib import admin
from django.conf import settings
from ctschool.ctpeople.models import Person


class NewsEntry(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Title")
    content = models.TextField(verbose_name = "Content")
    author = models.ForeignKey(Person, verbose_name = "Author")
    published = models.DateTimeField(verbose_name = "Published", auto_now_add=True)
    language = models.CharField(verbose_name = "Language",
        max_length=5,
        choices=settings.LANGUAGES,
        default=settings.LANGUAGE_CODE
    )
    def getauthor(self):
        return "%s %s" % (self.author.name, self.author.surname)
	
class NewsEntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'published', 'language')	
    class Media:
        js = ("/static/js/tiny_mce/tiny_mce.js",
              "/static/js/tiny_mce/textareas.js",)

admin.site.register(NewsEntry, NewsEntryAdmin)