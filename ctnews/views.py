from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Template, Context
from django.shortcuts import render_to_response
from django.contrib.flatpages.models import FlatPage
from ctschool.ctnews.models import NewsEntry
from ctschool.ctevents.models import Event
from ctschool.ctbooks.models import Book
from ctschool.ctfriends.models import Friend

def index(request):
    icontext = Context()
    newsEntries = NewsEntry.objects.all().order_by('-published')
    icontext['layouttype'] = 'fixed'
    icontext['news'] = newsEntries;
    events = Event.objects.all()
    icontext['events'] = events;
    books = Book.objects.all()
    icontext['books'] = books;
    pages = FlatPage.objects.all().exclude(url="""/contact/""")
    icontext['pages'] = pages;	
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;
    return render_to_response('index.html', icontext)
