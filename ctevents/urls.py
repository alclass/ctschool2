from django.conf import settings
from django.conf.urls.defaults import *

#events
urlpatterns = patterns('',
	url(r'^$', 'ctschool.ctevents.views.event_list', name='event_list'),		
    url(r'^(?P<eventID>\d+)/$', 'ctschool.ctevents.views.event_detail', name='event_detail'),	
	url(r'^(?P<eventID>\d+)/schedule/$', 'ctschool.ctevents.views.event_schedule', name='event_schedule'),	
	url(r'^(?P<eventID>\d+)/participants/$', 'ctschool.ctevents.views.event_participants', name='event_participants'),	
	url(r'^(?P<eventID>\d+)/gallery/$', 'ctschool.ctevents.views.event_gallery', name='event_gallery'),	
)