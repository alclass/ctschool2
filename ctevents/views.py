from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Template, Context
from django.shortcuts import render_to_response
from ctschool.ctevents.models import Event
from ctschool.ctfriends.models import Friend

def event_list(request):
    icontext = Context()
    events = Event.objects.all()
    icontext['events'] = events;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    return render_to_response('event_list.html', icontext)
	


def event_detail(request, eventID):
    icontext = Context()
    event = Event.objects.get(id=eventID)
    icontext['event'] = event;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    icontext['layouttype'] = 'fixed'
    return render_to_response('event_detail.html', icontext)

def event_schedule(request, eventID):
    icontext = Context()
    event = Event.objects.get(id=eventID)
    icontext['event'] = event;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;
    icontext['layouttype'] = 'fixed'	
    return render_to_response('event_schedule.html', icontext)
	
def event_gallery(request, eventID):
    icontext = Context()
    event = Event.objects.get(id=eventID)
    icontext['event'] = event;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;
    icontext['layouttype'] = 'fixed'	
    return render_to_response('event_gallery.html', icontext)
	
def event_participants(request, eventID):
    icontext = Context()
    event = Event.objects.get(id=eventID)
    icontext['event'] = event;  
    genspo = None
    genspons = Friend.objects.filter(isGeneralSponsor=True)
    if len(genspons) > 0:
        genspo = genspons[0]
    sponsors = Friend.objects.filter(isSponsor=True).filter(isGeneralSponsor=False)
    friends = Friend.objects.filter(isFriend=True)
    icontext['generalsponsor'] = genspo;
    icontext['sponsors'] = sponsors;
    icontext['friends'] = friends;	
    icontext['layouttype'] = 'fixed'
    return render_to_response('event_participants.html', icontext)