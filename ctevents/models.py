from django.db import models
from django.contrib import admin
from django.conf import settings
from ctschool.ctpeople.models import Participant
from ctschool.ctpeople.models import TeamMember
from photologue.models import Gallery
from django.template.defaultfilters import slugify

	
class TimeSlice(models.Model):
    start = models.TimeField(verbose_name = "Start time")
    end = models.TimeField(verbose_name = "End time")
    title = models.CharField(max_length=200, verbose_name = "Title")
    desc = models.TextField(verbose_name = "Description")
    person = models.ForeignKey(TeamMember, verbose_name = "Assigned person")
    def __str__(self):
        return self.start.strftime("%H:%M") +  "-" + self.end.strftime("%H:%M") +  " " + self.title
	
class TimeSliceAdmin(admin.ModelAdmin):
    list_display = ('start', 'end', 'title', 'desc', 'person')	
    class Media:
        js = ("/static/js/tiny_mce/tiny_mce.js",
              "/static/js/tiny_mce/textareas.js",)

class DaySchedule(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Short day description")
    date = models.DateField(verbose_name = "Date")
    slices = models.ManyToManyField(TimeSlice, related_name='schedules', verbose_name = "Time slices")
    def __str__(self):
        return self.date.strftime("%d.%m.%Y") + " " + self.title	
	
class DayScheduleAdmin(admin.ModelAdmin):
    list_display = ('date', 'title')	
			  
class Event(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Event title")
    shorttitle = models.CharField(max_length=25, verbose_name = "Short title")
    start_date = models.DateField(verbose_name = "Start date")
    end_date = models.DateField(verbose_name = "End date")
    mainorganiser = models.ForeignKey(TeamMember, verbose_name = "Main organiser")	
    participants = models.ManyToManyField(Participant, related_name='pars', verbose_name = "Participants", blank=True)
    schedules = models.ManyToManyField(DaySchedule, related_name='scheds', verbose_name = "Daily Schedules", blank=True)
    desc = models.TextField(verbose_name = "Description", blank=True)
    gallery = models.ForeignKey(Gallery, verbose_name = "Gallery", blank=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
		return "/event/%i/" % self.id
    def save(self, *args, **kwargs):
        if not self.pk:
            self.gallery = Gallery.objects.create(title=self.title + ' - event gallery', title_slug = slugify(self.title + ' - event gallery'), is_public=False)
        super(Event, self).save(*args, **kwargs)



class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'shorttitle', 'start_date', 'end_date', 'mainorganiser')	
    fieldsets = (
		('Instructions', {	
			'fields': (),	
			'classes': ('collapse',),			
			'description': '<ul><li>On this page You can define new of change existing event.</li><li>Event short name will appear on main page on this site.</li><li>When You create new event, new photo gallery will be automatically created with name same as name of new event. </li><li> Pictures You add to this gallery will appear on Event->Gallery page. </li><li>Event details from \'Advances Options\' can be left empty.</li><li> You can change ALL options like description or list of participants whenever you want. </li><li>It\'s YOUR responsibility to enter valid data. </li><li>Enter date in format YYYY-MM-DD For Example: 2008-06-25</li></ul>',
			}),
        ('Basic Options', {
			'fields': ('title', 'shorttitle','start_date','end_date','mainorganiser'),			
			'description': 'These options have to be filled to create a new event',
			}),
		('Advanced Options', {
			'fields': ('schedules','desc','participants'),
			'description': 'You can fill this fields later',
			}),
    )
    class Media:
        js = ("/static/js/tiny_mce/tiny_mce.js",
              "/static/js/tiny_mce/textareas.js",)	
	
admin.site.register(Event, EventAdmin)
admin.site.register(DaySchedule, DayScheduleAdmin)
admin.site.register(TimeSlice, TimeSliceAdmin)