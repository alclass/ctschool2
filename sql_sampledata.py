﻿# file: sql_sampledata.py
# -*- coding: utf-8 -*-
# Script inserts sample data to SQLite database file created after "manage.py syncdb" cmd issued

import sys
import sqlite3

#Environment dependent settings
siteDomain = '127.0.0.1:8000'
siteName = 'LocalTestServer'

def ExecuteSql(sqlQueries):
    connection = sqlite3.connect('ctschool.db')
    cursor = connection.cursor()
    for query in sqlQueries:
        cursor.execute(query)
    connection.commit()

def InsertAdminUser():
    print "Inserting admin account: jox/jox"
    sqlQueries = ["""INSERT INTO "auth_user" VALUES(1,'jox','','','jox@jox.jox','sha1$d80a8$c30cc2a86a8c72959112932bd5bc0ded3b28aa0d',1,1,1,'2008-09-15 11:16:54.998000','2008-08-28 15:44:51.751000');"""]
    ExecuteSql(sqlQueries)
    
def InsertBooks():
    print "Inserting sample Books"
    sqlQueries = ["""INSERT INTO "ctbooks_book" VALUES(1,'Kompjuterizovana Tomografija Centralnog Nervnog Sistema','Urednik: Prof. dr Sanja Stojanović','CT CNS, 2008',2008,'Autori knjige su redovni predavaci novosadske škole kompjuterizovane tomografije, predavaci na novosadskom i beogradskom Univerzitetu kao i strucnjaci sa velikim iskustvom u ovoj oblasti radiologije.Knjigu mozete naruciti preko e-mail adresa: rtgssns@gmail.com , rtgssns@eunet.rs ili na telefon +381 21 520 577');""",
    """INSERT INTO "ctbooks_book_authors" VALUES(1,1,2);"""]
    ExecuteSql(sqlQueries)

def InsertFacilities():
    print "Inserting sample Facilities"
    sqlQueries = ["""INSERT INTO "ctpeople_medicalfacility" VALUES(1,'Institut za Radiologiju, Klinicki Centar Vojvodine, Novi Sad');""",
                  """INSERT INTO "ctpeople_medicalfacility" VALUES(2,'Institut za Radiologiju, Klinicki Centar Srbije, Beograd');""",
                  """INSERT INTO "ctpeople_medicalfacility" VALUES(3,'Opsta Bolnica "Zoran Jovanovic", Zrenjanin');"""]
    ExecuteSql(sqlQueries)
    
def InsertPeople():
    print "Inserting sample Persons"
    sqlQueries = ["""INSERT INTO "ctpeople_person" VALUES(1,'dr.','Viktor','Till','1960-08-28',1,5,'',1,0,1);""",
                  """INSERT INTO "ctpeople_person" VALUES(2,'dr.','Sanja','Stojanovic','1960-08-28',1,4,'',1,0,1);""",
                  """INSERT INTO "ctpeople_person" VALUES(3,'dr.','Stevan','Idjuski','1960-08-28',1,1,'',1,0,1);""",
                  """INSERT INTO "ctpeople_person" VALUES(4,'dr.','Tanja','Opincal','1960-08-28',1,3,'',1,0,1);""",
                  """INSERT INTO "ctpeople_person" VALUES(5,'dr.','Dragoslav','Nenezic','1960-08-28',1,2,'',1,0,1);"""]
    sqlQueries += ["""INSERT INTO "ctpeople_teammember" VALUES(1,1,'<p>Doktorirao 1999. CT dijagnostikom se bavi od 1994. Boravio u Erlangenu 1997.god. na usavr&scaron;avanju na spiralnom CT-u. Pohadjao brojne seminare u inostranstvu. Bavi se CT angiografijom.  Pohadjao kurs u Davosu 2007. iz torakalne dijagnostike. &Scaron;ef odseka za plućnu dijagnostiku.</p>');""",
                  """INSERT INTO "ctpeople_teammember" VALUES(2,1,'<p>Osnivač i voditelj seminara. Na Institutu za radilogiju od 1991. Položila specijalistički ispit i odbranila magistarski rad 1994. Doktorat odbranila 1999. Pohadjala dva kursa iz abdominalne dijagnostike 2002. i 2006. kao i kurs iz CT dijagnostike glave i vrata 2000. u Davosu. Uvela transrektalni ultrazvuk prostate u kliničku praksu 1994. i TRUS vodjene biopsije prostate. Sa dr Idju&scaron;kim uvela metodu CT koronarografije 2005. u kliničku praksu. Nalazi se na položaju upravnika Instituta za radiologiju KCV.</p>');""",
                  """INSERT INTO "ctpeople_teammember" VALUES(3,1,'<p>Načelnik odeljenja za CT dijagnostiku Instituta za radiologiju KC NS. Pionir u angiografijama u oblasti neuroradiologije, a od 2005. i u CT koronarografijama. Usavr&scaron;avao se u vi&scaron;e stranih i domaćih centara. Polaznik kursa iz CNS 1997. u Davosu.</p>');""",
                  """INSERT INTO "ctpeople_teammember" VALUES(4,1,'<p>Specijalizaciju iz radiologije zavr&scaron;io 1991.godine. Edukovan za CT dijagnostiku tela, vrata i masiva lica u KCS-u i Solunu. CT dijagnostikom se bavi od 1995. godine.   Formirao je službu za CT dijagnostiku i edukovao sve nivoe osoblja u Pri&scaron;tini i Foči. &Scaron;ef je predmeta Radiologija sa nuklearnom medicinom pri Medicinskom fakultetu Univerziteta Crne Gore. Predsednik je Udruženja radiologa Crne Gore.</p>');""",
                  """INSERT INTO "ctpeople_teammember" VALUES(5,1,'<p>Specijalizaciju iz radiologije zavr&scaron;io 1991.godine. Edukovan za CT dijagnostiku tela, vrata i masiva lica u KCS-u i Solunu. CT dijagnostikom se bavi od 1995. godine.   Formirao je službu za CT dijagnostiku i edukovao sve nivoe osoblja u Pri&scaron;tini i Foči. &Scaron;ef je predmeta Radiologija sa nuklearnom medicinom pri Medicinskom fakultetu Univerziteta Crne Gore. Predsednik je Udruženja radiologa Crne Gore.</p>');"""]
    ExecuteSql(sqlQueries)            

    
def InsertNews():
    print "Inserting sample News"
    sqlQueries = ["""INSERT INTO "ctnews_newsentry" VALUES(1,'U prodaji je knjiga CT CNS-a u izdanju novosadske CT škole ','<p style="text-align: center;"><strong>Urednik: Prof. dr Sanja Stojanovic </strong></p><p class="style27" align="justify"><img src="../../../../static/photologue/photos/knjiga.jpg" border="0" width="109" height="164" />Autori knjige su redovni predavaci novosadske &scaron;kole kompjuterizovane tomografije, predavaci na novosadskom i beogradskom Univerzitetu kao i strucnjaci sa velikim iskustvom u ovoj oblasti radiologije.</p><p class="style27" align="justify">Vi&scaron;e informacija o knjizi možete naći <a href="../../../../book/1/" target="_blank">ovde</a></p><p class="style27" align="justify">Knjigu mozete naruciti preko e-mail adresa: <a href="mailto:rtgssns@gmail.com" target="_blank">rtgssns@gmail.com</a> , <a href="mailto:rtgssns@eunet.rs" target="_blank">rtgssns@eunet.rs</a> ili na telefon +381 21 520 577</p>',2,'2008-08-21 12:47:45.326000','en');""",
                  """INSERT INTO "ctnews_newsentry" VALUES(2,'Šesta škola kompjuterizovane tomografije','<p style="text-align: center;"><strong> Organizatori &scaron;kole:</strong></p><p style="text-align: center;"><strong><a href="http://www.ctschool.eu/centar.html" target="_blank"></a></strong></p><p style="text-align: center;"><strong><a href="http://www.ctschool.eu/centar.html" target="_blank"> Centar za radiologiju Klinickog centra Vojvodine </a></strong> <strong><a href="http://www.medical.ns.ac.yu/" target="_blank"></a></strong></p><p style="text-align: center;"><strong><a href="http://www.medical.ns.ac.yu/" target="_blank"> Centar za kontinuiranu edukaciju u zdrvastvu Medicinskog fakulteta Novi Sad </a></strong></p><p><img src="../../../../static/photologue/photos/sat.jpg" border="0" width="180" height="135" /> Edukativni seminar iz CT dijagnostike je predviden za edukaciju lekara na specijalizaciji iz radiologije, ali i specijalista radiologije koji žele da nadograde i usavr&scaron;e svoje znanje u ovoj oblasti.</p><p class="style27" align="justify">Predavaci su iskusni radiolozi iz oblasti CT dijagnostike iz na&scaron;e zemlje i vrhunski, svetski poznati radiolozi, iz inostranstva.</p>',3,'2008-08-22 12:31:54.918000','en');"""]
    ExecuteSql(sqlQueries)                  


def InsertBooksAuthors():
    print "Inserting sample Books<->Authors connections"
    sqlQueries = ["""INSERT INTO "ctbooks_book_authors" VALUES(1,1,1);""",
                  """INSERT INTO "ctbooks_book_authors" VALUES(2,1,2);""",
                  """INSERT INTO "ctbooks_book_authors" VALUES(3,2,3);"""]
    ExecuteSql(sqlQueries)


def InsertPhotoSizes():
    print "Inserting sample Photo Sizes"
    sqlQueries = ["""INSERT INTO "photologue_photosize" VALUES(1,'thumbnail',64,64,70,0,0,0,0,NULL,NULL);""",    
    """INSERT INTO "photologue_photosize" VALUES(2,'display',96,0,70,0,0,0,0,NULL,NULL);""",
    """INSERT INTO "photologue_photosize" VALUES(3,'friend',165,0,70,1,0,0,0,NULL,NULL);""",
    """INSERT INTO "photologue_photosize" VALUES(4,'admin_thumbnail',64,0,70,0,0,0,0,NULL,NULL);""",
	"""INSERT INTO "photologue_photosize" VALUES(5,'gallerythumb',55,70,70,0,0,0,0,NULL,NULL);"""]
    ExecuteSql(sqlQueries)

    
def InsertPhotos():
    print "Inserting sample Photos"
    sqlQueries = ["""INSERT INTO "photologue_photo" VALUES(1,'photologue/photos/idjuski.jpg','2008-08-26 12:56:29.056000',0,'center',NULL,'Stevan Idjuski','stevan-idjuski','dr. Stevan Idjuski','2008-08-26 12:56:28.728000',1,'idjuski');""",
                  """INSERT INTO "photologue_photo" VALUES(2,'photologue/photos/nenezic.jpg','2008-08-26 12:56:56.681000',0,'center',NULL,'Dragoslav Nenezic','dragoslav-nenezic','dr. Dragoslav Nenezic','2008-08-26 12:56:56.681000',1,'nenezic');""",
                  """INSERT INTO "photologue_photo" VALUES(3,'photologue/photos/opincal.jpg','2008-08-26 12:57:18.697000',0,'center',NULL,'Tanja Opincal','tanja-opincal','dr. Tanja Opincal','2008-08-26 12:57:18.681000',1,'opincal');""",
                  """INSERT INTO "photologue_photo" VALUES(4,'photologue/photos/stojanovic.jpg','2008-08-26 12:57:42.838000',0,'center',NULL,'Sanja Stojanovic','sanja-stojanovic','dr. Sanja Stojanovic','2008-08-26 12:57:42.838000',1,'stojanovic');""",
                  """INSERT INTO "photologue_photo" VALUES(5,'photologue/photos/till.jpg','2008-08-26 12:57:58.150000',0,'center',NULL,'Viktor Till','viktor-till','dr. Viktor Till','2008-08-26 12:57:58.150000',1,'till');""",
                  """INSERT INTO "photologue_photo" VALUES(6,'photologue/photos/bayer.gif','2008-08-26 12:59:58.384000',0,'center',NULL,'Bayer','bayer','','2008-08-26 12:59:58.384000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(7,'photologue/photos/ge.gif','2008-08-26 13:00:07.119000',0,'center',NULL,'General Eletrics','general-eletrics','','2008-08-26 13:00:07.103000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(8,'photologue/photos/jugohemija.jpg','2008-08-26 13:00:13.963000',0,'center',NULL,'Jugohemija','jugohemija','','2008-08-26 13:00:13.963000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(9,'photologue/photos/medicom.jpg','2008-08-26 13:00:21.634000',0,'center',NULL,'Medicom','medicom','','2008-08-26 13:00:21.619000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(10,'photologue/photos/nlbbanka.gif','2008-08-26 13:00:32.025000',0,'center',NULL,'NLB Continental Banka','nlb-continental-banka','','2008-08-26 13:00:32.025000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(11,'photologue/photos/siemens.jpg','2008-08-26 13:00:40.275000',0,'center',NULL,'SIEMENS medical Belgrade','siemens-medical-belgrade','','2008-08-26 13:00:40.275000',1,'');""",
                  """INSERT INTO "photologue_photo" VALUES(12,'photologue/photos/poster5.jpg','2008-08-26 13:39:06.041000',0,'center',NULL,'poster2008','poster2008','','2008-08-26 13:39:04.697000',1,'poster2008');""",
                  """INSERT INTO "photologue_photo" VALUES(13,'photologue/photos/schering.jpg','2008-09-17 19:23:56.627000',0,'center',NULL,'Schering Serbia','schering-serbia','','2008-09-17 19:23:56.566000',0,'');""",
                  """INSERT INTO "photologue_photo" VALUES(14,'photologue/photos/sat.jpg','2006-11-19 13:22:12',0,'center',NULL,'Novi Sad - Sat','novi-sad-sat','','2008-09-17 20:00:04.780000',0,'sat');""",
                  """INSERT INTO "photologue_photo" VALUES(15,'photologue/photos/knjiga.jpg','2008-09-18 12:23:12.419000',0,'center',NULL,'CTCNS','ctcns','','2008-09-18 12:23:12.138000',0,'');""",
                  """INSERT INTO "photologue_photo" VALUES(16,'photologue/photos/mapa.jpg','2008-09-18 12:34:37.309000',0,'center',NULL,'mapa','mapa','','2008-09-18 12:34:35.824000',1,'');"""]
    ExecuteSql(sqlQueries)

def InsertFriends():
    print "Inserting sample Friends"
    sqlQueries = ["""INSERT INTO "ctfriends_friend" VALUES(1,'SIEMENS medical Belgrade','http://www.siemens.co.yu/med',1,0,1,11);""",
                  """INSERT INTO "ctfriends_friend" VALUES(2,'GE-General Electrics','http://www.gehealthcare.com/eueu/index.html',1,0,0,7);""",
                  """INSERT INTO "ctfriends_friend" VALUES(3,'NLB Continental Banka','http://www.blic.co.yu/',1,0,0,10);""",
                  """INSERT INTO "ctfriends_friend" VALUES(4,'BAYER SCHERING PHARMA Serbia','http://www.schering.co.yu/',1,0,0,6);""",
                  """INSERT INTO "ctfriends_friend" VALUES(5,'Jugohemija d.o.o.','http://www.blic.co.yu/',1,0,0,8);""",
                  """INSERT INTO "ctfriends_friend" VALUES(6,'SCHERING PHARMA Serbia','http://www.schering.co.yu/',0,1,0,13);"""]
    ExecuteSql(sqlQueries)

    
def InsertGalleries():
    print "Inserting sample Galleries"
    sqlQueries = ["""INSERT INTO "photologue_gallery" VALUES(1,'2008-08-26 12:53:14','Team','team','Team photos',1,'team');""",
                  """INSERT INTO "photologue_gallery" VALUES(2,'2008-08-26 12:53:56','Participants','participants','Participants photos',1,'participants');""",
                  """INSERT INTO "photologue_gallery" VALUES(3,'2008-08-26 12:54:28','SitePhotos','sitephotos','',1,'stuffphotos');""",
                  """INSERT INTO "photologue_gallery" VALUES(4,'2008-08-26 12:54:58','Friends','friends','Friends & Sponsors gallery',1,'friends');""",
                  """INSERT INTO "photologue_gallery" VALUES(5,'2008-09-18 15:47:32.736000','CT School 2008 - event gallery','ct-school-2008-event-gallery','',0,'');"""]
    ExecuteSql(sqlQueries)    
	
def InsertEvents():
    print "Inserting sample Events"
    sqlQueries = ["""INSERT INTO "ctevents_event" VALUES(1,'Sesta Skola kompjuterizovane tomografije','6. CT School - 2008','2008-11-10','2008-11-15',2,'<div class="post"><p class="style27" align="justify"><img src="../../../../static/photologue/photos/poster5.jpg" border="0" width="169" height="235" />Edukativni seminar iz CT dijagnostike je predviđen za edukaciju lekara na specijalizaciji iz radiologije, ali i specijalista radiologije koji žele da nadograde i usavr&scaron;e svoje znanje u ovoj oblasti.</p><p class="style27" align="justify">Predavači su iskusni radiolozi iz oblasti CT dijagnostike iz na&scaron;e zemlje i vrhunski, svetski poznati radiolozi, iz inostranstva.</p></div><p class="style27" align="center">&nbsp;</p><p class="style27" align="center">&nbsp;</p><h3 style="text-align: center;">TEME koje se biti obradjene za vreme trajanja skole</h3><p class="style19" style="text-align: center;"><strong>CT DIJAGNOSTIKA GLAVE, VRATA I KIČMENOG STUBA<br />Moderator: Prof. dr Sanja Stojanović</strong></p><ul><li class="style27">CT dijagnoza i evaluacija tumora mozga</li><li class="style27">CT cerebrovaskularnih oboljenja</li><li class="style27">CT angiografija</li><li class="style27">CT inflamatornih i degenerativnih oboljenja</li><li class="style27">CT maksilofacijalne regije i temporalne kosti</li><li class="style27">CT vrata</li><li class="style27">CT kičmenog stuba</li><li class="style27">CT pedijatrijskog CNS-a</li><li class="style27">CT u traumi glave, vrata i kičmenog stuba</li><li class="style27">CT osteomuskularnog sistema</li><li class="style27">Primena CT-a  u interventnoj radiologiji</li></ul><p class="style25" align="justify">&nbsp;</p><p class="style19" style="text-align: center;"><strong>CT DIJAGNOSTIKA TORAKSA<br />Moderator: Prof. dr Viktor Till </strong></p><ul><li class="style27">Osnovni principi CT dijagnostike toraksa </li><li class="style27">CT medijastinuma </li><li class="style27">CT u zapaljenskim promenama pluća </li><li class="style27">CT u tumorskim promenama pluća </li><li class="style27">CT u dijagnostici oboljenja pleure </li><li class="style27">CT u dijagnostici oboljenja torakalnog zida </li><li class="style27">CT u dijagnostici oboljenja dijafragme </li><li class="style27">CT u traumi i urgentnim stanjima toraksa </li><li class="style27">CT-om vodjene intervencije u toraksu </li><li class="style27">Doprinos CT-a  u kardiologiji </li></ul><p>&nbsp;</p><p class="style19" style="text-align: center;"><strong>CT U DIJAGNOSTICI   					OBOLJENJA ABDOMENA<br />Moderator: Prof. dr Du&scaron;an Hadnađev</strong></p><ul class="style27"><li>Osnovni principi u CT dijagnostici abdomena </li><li>CT u dijagnostici jetre i bilijarnog stabla </li><li>CT u dijagnostici oboljenja pankreasa </li><li>CT u dijagnostici oboljenja slezine </li><li>CT u dijagnostici oboljenja bubrega </li><li>CT u dijagnostici oboljenja nadbubrega </li><li>CT u dijagnostici oboljenja karlice </li><li>CT vaskularnih struktura retroperitoneuma </li><li>CT u traumi i urgentnim stanjima abdomena </li></ul>',5);"""]
    ExecuteSql(sqlQueries)  

def InsertPhotoGalleries():
    print "Inserting sample Photo<->Galleries"
    sqlQueries = ["""INSERT INTO "photologue_gallery_photos" VALUES(1,1,1);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(2,1,2);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(3,1,3);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(4,1,4);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(5,1,5);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(6,4,6);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(7,4,7);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(8,4,8);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(9,4,9);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(10,4,10);""",
                  """INSERT INTO "photologue_gallery_photos" VALUES(11,4,11);"""]
    ExecuteSql(sqlQueries)

def InsertFlatPages():
    print "Inserting sample FlatPages"
    sqlQueries = ["""INSERT INTO "django_flatpage" VALUES(1,'/contact/','KONTAKT','<h4 class="style19" style="text-align: center;">Adresa:</h4>

<p style="text-align: center;"><strong>Klinički Centar Vojvodine</strong>, CENTAR ZA RADIOLOGIJU<br /><strong>(sa naznakom za CT seminar)</strong> <strong>Hajduk Veljkova 1-7</strong> <br class="style27" /> <strong>21000 Novi Sad, Vojvodina, Srbija</strong></p>

<p style="text-align: center;"><img src="../../../../static/photologue/photos/mapa.jpg " border="0" alt="mapa" width="522" height="426" /></p>

<p style="text-align: center;">&nbsp;</p>

<h4 class="style19" style="text-align: center;">Telefon i fax:</h4>

<p style="text-align: center;"><strong>+381-21-520-577</strong> (direktan telefon)           <br class="style27" /> <strong>+381-21-484-3-484&nbsp; lokali: 3142 ; 3135 </strong> <br class="style27" /> <strong>+381-63-77-55-983               &nbsp;*MOB.-Ass.dr Kosta Petrović</strong> <br class="style27" /> <strong>fax +381-21-520-577</strong></p>

<p>&nbsp;</p>

<p style="text-align: center;"><strong>

<h4 class="style19" style="text-align: center;">e-mail:</h4>

<a href="mailto:rtgssns@gmail.com" target="_blank">rtgssns@gmail.com</a></strong><br /> <strong><a href="mailto:rtgssns@eunet.rs" target="_blank">rtgssns@eunet.rs</a></strong></p>',0,'',0);""",    
                  """INSERT INTO "django_flatpage" VALUES(2,'/testpage/','Test strana','<p>Ovo je test strana, i ako upali bice EXTRA!!!</p>',0,'',0);""",
                  """INSERT INTO "django_flatpage_sites" VALUES(1,1,1);""",
                  """INSERT INTO "django_flatpage_sites" VALUES(2,2,1);"""]
    ExecuteSql(sqlQueries)
    
def UpdateSites():
    print "Updating sample Sites"
    sqlQueries = ["""UPDATE "django_site" SET domain = '""" + siteDomain + """', name='""" + siteName + """' WHERE id=1;"""]
    ExecuteSql(sqlQueries)

def main(args):
    if len(args) == 0:        
        DoAll()
    else:
        if 'admin' in args:
            InsertAdminUser()
        if 'facs' in args:
            InsertFacilities()
        if 'photos' in args:
            InsertPhotoSizes()
            InsertGalleries()
            InsertPhotos()            
            InsertPhotoGalleries()
        if 'people' in args:
            InsertPeople()
        if 'friends' in args:        
            InsertFriends()
        if 'news' in args:
            InsertNews()
        if 'pages' in args:
            InsertFlatPages()
        if 'events' in args:
            InsertEvents()
        if 'books' in args:
            UpdateBooks()
        if 'sites' in args:
            UpdateSites()
            
        
def DoAll():
    print 'Script called without arguments. Doing complete insertion.'
    InsertAdminUser()
    InsertFacilities()
    InsertPhotoSizes()
    InsertPhotos()    
    InsertGalleries()
    InsertPhotoGalleries()
    InsertPeople()
    InsertFriends()
    InsertFlatPages()
    InsertNews()
    InsertBooks()
    InsertEvents()
    UpdateSites()
    
if __name__ == '__main__':
    main(sys.argv[1:])